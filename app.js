let message = process.argv[2];

const figlet = require('figlet');

figlet.text(message, (error, data) => {
    if (error) {
        console.log(error);
    } else {
        console.log(data);
    }
});